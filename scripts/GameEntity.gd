extends KinematicBody2D
class_name GameEntity


export  var slide = 1
#loudness var set energy of soundburst
export var walkingLoudness = 0.5
var walkInterval = 0.5


#target var tell player character where to go/ where to throw
var walkTarget:Vector2
var throwTarget: Vector2
#velocity var tells PC what direction to go in
var velocity:Vector2
var animationState: String = "Idle"

var myEchoMaker = 	EchoMaker.new()


func _ready():
	pass
	

func WalkSound(soundType):
#if moving make sound every 1 sec between 0.50 and 0.49 second mark
	if $Timer.get_time_left() > (walkInterval-.01) && $Timer.get_time_left() < walkInterval && global_position.distance_to(walkTarget) > 5:
#calls the newEcho function to create a soundburst at position with set loudness
		get_parent().add_child(myEchoMaker.newEcho(global_position,walkingLoudness,soundType))

		

func 	Walk(speed):
#move toward walk target
	var veloc = global_position.direction_to(walkTarget) * speed
	if global_position.distance_to(walkTarget) > 5:
		velocity = move_and_slide(veloc,Vector2(0.0,0.0),false,slide)
		animationState = "Walk"
	if global_position.distance_to(walkTarget) <= 5 and animationState != "Throw":
		animationState = "Idle"
