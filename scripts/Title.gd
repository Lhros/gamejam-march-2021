extends Node2D


var Interval = 0.5
var myEchoMaker = EchoMaker.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start()

	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	
	if $Timer.get_time_left() > (Interval -0.02) && $Timer.get_time_left() < Interval:
		get_child(1).add_child(myEchoMaker.newEcho(global_position,1,"PLOPSOUND"))
		get_child(1).get_child(0).areaCheck = true



func _on_Play_button_down():
	get_tree().change_scene("res://Main Scene.tscn")
