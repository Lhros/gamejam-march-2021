extends Area2D
class_name SoundBurst


var streamLength:float
var loudness :float
var soundType :String
#all the sounds that need
var walkSound = load("res://sounds/stonewalking01.wav")
var throwSound = load("res://sounds/stonewalking01.wav")
var plop1 = load("res://sounds/plop1.wav")
var plop2 = load("res://sounds/plop2.wav")
var plop3 = load("res://sounds/plop3.wav")
var waterPlayer = load("res://sounds/waterCaver.wav")
var waterGremlin =load("res://sounds/waterGremlin.wav")
var boulderBreak = load("res://sounds/BoulderBreak.wav")
var doorLocked =  load("res://sounds/Doorlocked.wav")
var unlocked =  load("res://sounds/Unlock.wav")
var pickUpKey =  load("res://sounds/keyPickup.wav")
var keyJingle =  load("res://sounds/keyjingle.wav")

var plopSound
var randPlop = RandomNumberGenerator.new()


onready var nav2D = get_node('../Navigation2D')

var areaCheck:bool = false
var ShapeCollider = 0
var WallLight = 1
var FloorLight = 2
	
func _ready():
	AudioBurst()
	$Timer.set_wait_time($AudioStreamPlayer2D.get_stream().get_length())
	$Timer.start()
	
	get_child(WallLight).scale = Vector2(0.0,0.0)
	get_child(FloorLight).scale = Vector2(0.0,0.0)

func _process(_delta):
	SoundBurst()
#this timer is ancient but it works so im not touching it
	if $Timer.get_time_left() == 0 and areaCheck == true:
		queue_free()
#we wanted random plop sounds so this makes the plop sounds random
func RandomPlop():
	randPlop.randomize()
	match randPlop.randi_range(1,3):
		1:
			plopSound = plop1
			#loudness = 0.5
		2:
			plopSound = plop2
			#loudness = 0.5
		3:
			plopSound = plop3
			#loudness = 0.5
		_:
			return
func AudioBurst():
	#match soundtype to sound file
	match soundType:
		"WATERPLAYERWALKSOUND":
			$AudioStreamPlayer2D.set_stream(waterPlayer)
		"WATERTHROWSOUND":
			$AudioStreamPlayer2D.set_stream(plop2)
		"PLAYERWALKSOUND":
			$AudioStreamPlayer2D.set_stream(walkSound)
		"GREMLINWALKSOUND":
			$AudioStreamPlayer2D.set_stream(walkSound)
		"THROWSOUND":
			$AudioStreamPlayer2D.set_stream(throwSound)
		"PLOPSOUND":
			RandomPlop()
			$AudioStreamPlayer2D.set_stream(plopSound)
		"BOULDERBREAK":
			$AudioStreamPlayer2D.set_stream(boulderBreak)
		"DOORLOCKED":
			$AudioStreamPlayer2D.set_stream(doorLocked)
		"UNLOCKED":
			$AudioStreamPlayer2D.set_stream(unlocked)
		"PICKUPKEY":
			$AudioStreamPlayer2D.set_stream(pickUpKey)
		"KEYJINGLE":
			$AudioStreamPlayer2D.set_stream(keyJingle)
		_:
			$AudioStreamPlayer2D.set_stream(plopSound)
			
	
#set stream length var to length of stream. we use this variable to determine the rate of energy decay for each sound burst
	streamLength = $AudioStreamPlayer2D.get_stream().get_length()
#loudness will tell us how far you can hear a sound and how loud it is. otherwise this var has lost its usefulness
	$AudioStreamPlayer2D.set_max_distance((750*loudness))
	$AudioStreamPlayer2D.set_volume_db(loudness)
#start the audio stream
	$AudioStreamPlayer2D._set_playing(true)
# set the radius for Area2D that checks for player and gremlin to be just beyond the cutoff threshold for each sound
	get_child(ShapeCollider).shape.radius = (750*loudness) + 1
# set the energy levels to a reasonavle value. I tried making these variable based on loudness/stream length but in the end it just wasnt working
	get_child(WallLight).energy = 0.4
	get_child(FloorLight).energy = 0.4
	



		
func SoundBurst():
	#scale the light based on stream length
	get_child(WallLight).scale += Vector2((1/(streamLength*60)),(1/(streamLength*60)))
	get_child(FloorLight).scale +=Vector2((1/(streamLength*60)),(1/(streamLength*60)))
# subtracts a fraction of our energy with a base of streamlength/180 because it just feels right.
	get_child(WallLight).energy -=(get_child(WallLight).energy/(streamLength*180))
	get_child(FloorLight).energy -=(get_child(WallLight).energy/(streamLength*180))


#tells us if the soundburst hit something if it hits the gremlin and that sound is a player walk sound or throw sound. we change the gremlinns pathfinding to that location
#then we set area check to true, which allows the stream to delete
func _on_Soundburst_body_entered(body):
	if soundType == "PLAYERWALKSOUND" or soundType == "THROWSOUND" && body.name == "Gremlin":
		get_node('../Gremlin').path = nav2D.get_simple_path(get_node('../Gremlin').global_position,global_position)
	areaCheck = true

	
	
