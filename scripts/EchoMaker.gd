extends Node
class_name EchoMaker

func newEcho(echoLocation: Vector2, newLoudness:float,soundType: String):
	var newSoundBurst = load("res://soundBurst.tscn")
	var EchoStep = newSoundBurst.instance()
	EchoStep.position = echoLocation
	EchoStep.loudness = newLoudness
	EchoStep.soundType = soundType
	return EchoStep
	


