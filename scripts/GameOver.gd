extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if get_node('../GameWin').gamewin == true:
		$Escaped.show()
	else:
		$Died.show()
		pass
	pass # Replace with function body.



func _process(_delta):
	if get_node('../Caver').DEADMODE ==true:
		self.show()



func _on_Replay_pressed():
	get_tree().change_scene("res://Main Scene.tscn")


func _on_Quit_pressed():
	get_tree().change_scene("res://Title.tscn")
	#get_tree().quit()
