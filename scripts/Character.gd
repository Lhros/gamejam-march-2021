extends GameEntity
class_name Character

export var speed = 50
export var throwLoudness = 0.5

var gameOver = load('res://GameOver.tscn')

var DEADMODE = false
var inWater = false
var hasKey = false

var throwSound = "THROWSOUND"
var playerWalkSound = "PLAYERWALKSOUND"

func _ready():
	pass
	
func _physics_process(_delta):
	PlayerState()


	
func PlayerState():
	if hasKey == true:
		##add key UI to camera
		#add jingle
		pass

	if inWater == false:
		playerWalkSound = "PLAYERWALKSOUND"
		throwSound = "THROWSOUND"
	else:
		playerWalkSound = "WATERPLAYERWALKSOUND"
		throwSound = "WATERTHROWSOUND"
	if DEADMODE == false:
		Walk(speed)
		HandleAnimation()
		WalkSound(playerWalkSound)
	if DEADMODE == true:
		$AnimatedSprite.set_animation("Death")
		var newGameOver =gameOver.instance()
		get_parent().add_child(newGameOver)
		newGameOver.position = position
		set_physics_process(false)

func _input(event):
	if DEADMODE == false:
		PlayerWalk(event)
		PlayerThrow(event)
	
func PlayerWalk(event):
	#on left click walk to location of click
	if event.is_action_pressed('click'):
			walkTarget = get_global_mouse_position()
			
func HandleAnimation():
	
	match animationState:
		"Throw":
			$AnimatedSprite.set_rotation( global_position.angle_to_point(throwTarget))
			$AnimatedSprite.play("Throw")
		"Walk":
			$AnimatedSprite.set_rotation( global_position.angle_to_point(walkTarget))
			$AnimatedSprite.play("Walk")
		"Idle":
			$AnimatedSprite.set_rotation( global_position.angle_to_point(walkTarget))
			$AnimatedSprite.play("Idle")
		

func PlayerThrow(event):
#throw a rock on right click
	if event.is_action_pressed('RightClick'):
#set rock target to mouse position
		throwTarget = get_global_mouse_position()
#set animation state to throw
		animationState = "Throw"
#raycast from position to target
		var spaceState = get_world_2d().direct_space_state
		var result = spaceState.intersect_ray(position,throwTarget)
#if the rock collides with a thing create sound burst at location of hit
		if result.has('position') == true:
			get_parent().add_child(myEchoMaker.newEcho(result.get('position'),throwLoudness,throwSound))
#If rock didnt hit thing create sound burst at location of click
		else:
			get_parent().add_child(myEchoMaker.newEcho(throwTarget,throwLoudness,throwSound))	
	elif $AnimatedSprite._is_playing() == false:
		animationState = "Idle"



func _on_EatArea_body_entered(body):
	if body.name == "Gremlin":
		print_debug("THE PLAYER WAS EATED")
		DEADMODE = true
