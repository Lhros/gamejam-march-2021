extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var myEchoMaker = 	EchoMaker.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(_delta):
	pass

func _on_Area2D_body_entered(body):

	if body.name == "Gremlin":
		$Timer.start()
		get_node('../Gremlin').atRock = true
		SoundHandle()
		
		$Area2D.queue_free()
		$StaticBody2D.queue_free()

func SoundHandle():
	get_parent().call_deferred("add_child", myEchoMaker.newEcho(global_position,1,"BOULDERBREAK"))
	
func _on_Area2D_body_exited(body):
	if body.name == "Gremlin":
		get_node('../Gremlin').atRock = false
	pass # Replace with function body.


func _on_Timer_timeout():
	$AnimatedSprite.play()
