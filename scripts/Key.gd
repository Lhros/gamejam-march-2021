extends Node2D

var Interval = 0.5
var Loudness = 1.0
var myEchoMaker = EchoMaker.new()
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start()



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if $Timer.get_time_left() > (Interval -0.01) && $Timer.get_time_left() < Interval && global_position.distance_to(get_node('../Caver').global_position) < (749.0*Loudness) :
		get_parent().add_child(myEchoMaker.newEcho(global_position,1,"KEYJINGLE"))


func _on_Area2D_body_entered(body):
	if body.name == "Caver":
		get_node('../Caver').hasKey = true
		get_parent().add_child(myEchoMaker.newEcho(global_position,1,"PICKUPKEY"))
		queue_free()
	pass # Replace with function body.
