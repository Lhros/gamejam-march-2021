extends GameEntity

export var speed : float = 40.0
var path : = PoolVector2Array() setget SetPath
var currentPoint = null
var atRock = false
var aniHandler:String = "Idle"

func _ready():
	walkInterval = .25
	set_process(false)


func _process(delta):
	var moveDistance : float = speed * delta
	MoveAlongPath(moveDistance)
	WalkSound("GREMLINWALKSOUND")
	HandleAnimation()
	
func HandleAnimation():
	if aniHandler == "Walk":
		$AnimatedSprite.set_rotation( global_position.angle_to_point(walkTarget))
		$AnimatedSprite.play("Gremlinwalk")


func SetPath(value: PoolVector2Array) -> void:
	path = value
	if value.size() == 0:
		return
	set_process(true)

		
func MoveAlongPath(distance : float) -> void:
	var startPoint : = position
	for _i in range(path.size()):
		walkTarget = path[0]
		var distanceToNextPoint = startPoint.distance_to(path[0]) 
		if distance <= distanceToNextPoint and distance >= 0.0 and atRock == false:
			position = startPoint.linear_interpolate(path[0], distance/distanceToNextPoint)
			aniHandler = "Walk"
			break

		distance -= distanceToNextPoint
		startPoint = path[0]
		path.remove(0)
		aniHandler = "Idle"


			
func IdleBehavior()-> void:
	pass
#Idle Behavior - select behavior at random, conduct behavior for random time,remove behavior from list, and select another behavior. 
#when all behaviors have been selected - reset, a sound event interupts behavior
# look around
# wait
# wander
#play

func foundSound()-> void:
	pass

#On sound event
#get sound type if sound type is a b or c
#"listen" wait 1-2 seconds
#	explore toward sound
# investigate sound point
# return to idle behavior

	
func PlayerDiscovery():
	pass
#on player discovery
# get excited
# run after player sound event 
# eat player
# if player is quiet - dont eat player - search for x time then either idle if no player or go back to eating
# if player  is out of range - search for x time then either idle if no player or go back to eating


