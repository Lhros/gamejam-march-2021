extends Node2D
var Loudness:float = 1.0
var DropEcho = EchoMaker.new()
var Interval = 0.3
var rng = RandomNumberGenerator.new()



func _ready():
	#creates random interval for each drop
	rng.set_seed(global_position.x)
	Interval = stepify(rng.randf_range(3.00,0.00),0.1)

func _physics_process(_delta):	
#if player is within 1000 spaces of node then create plop every 3 seconds
	if $Timer.get_time_left() > (Interval -0.01) && $Timer.get_time_left() < Interval && global_position.distance_to(get_node('../Caver').global_position) < (749.0*Loudness) :
		get_node('../').add_child(DropEcho.newEcho(global_position,Loudness,"PLOPSOUND"))
